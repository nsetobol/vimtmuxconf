syn on
set nu
map <C-h> 5h
map <C-j> 5j
map <C-k> 5k
map <C-l> 5l

let mapleader = " "
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab
set softtabstop=4
set nowrap
set noexpandtab
set copyindent
set autoindent

function! s:build()
	let &makeprg = './build.sh'
	silent make
	copen
	!clear
endfunction

command! Build call s:build()
set runtimepath^=~/.vim/bundle/ag
set runtimepath^=~/.vim/bundle/ag
